import React, { Component } from 'react';
import './App.css';
import { Button, BottomNavigation, BottomNavigationAction, Container, Typography } from '@material-ui/core';
var sayfalar = [];
var linkler = [];
var timer = null;
export default class App extends Component {
  state = { buttonDurum: "START", turList: <b><i>"Waiting for your steps to your dreams..."</i></b>, turSayi: 0, saat: "00:00", mola: 0, molaAyristir: 0 }
  timer = () => {
    if (this.state.buttonDurum === "START" || this.state.buttonDurum === "CONTINUE") {
      this.setState({ buttonDurum: "PAUSE", turList: <b><i>"Go ahead!"</i></b> });
      timer = setInterval(this.timeContinue, 1000);
    }
    else {
      clearInterval(timer);
      this.setState({ buttonDurum: "CONTINUE", turList: <b><i>"Waiting for your steps to your dreams..."</i></b> });
      document.title = "PAUSE";
    }
  }
  timeContinue = () => {
    if (Number(this.state.saat.split(":")[0]) < 25 && this.state.mola === 0) {
      this.setState({ turList: <b><i>"Go ahead!"</i></b> });
      this.timeIncrease("WORK: ");
    }
    else {
      if (this.state.molaAyristir === 0) {
        this.setState({ saat: "00:00", mola: 1, molaAyristir: 1, turSayi: this.state.turSayi + 1 });
      }
      if (this.state.turSayi % 4 === 0) {
        if (Number(this.state.saat.split(":")[0]) > 29) {
          this.setState({ saat: "00:00", mola: 0, molaAyristir: 0 });
        }
        else {
          this.setState({ turList: <b><i>"Take a break :)"</i></b> });
          this.timeIncrease("BREAK: ");
        }
      }
      else {
        if (Number(this.state.saat.split(":")[0]) > 4) {
          this.setState({ saat: "00:00", mola: 0, molaAyristir: 0 });
        }
        else {
          this.setState({ turList: <b><i>"Take a break :)"</i></b> });
          this.timeIncrease("BREAK: ");
        }
      }
    }
  }

  timeIncrease = (ney) => {
    if (Number(this.state.saat.split(":")[1]) < 59) {
      var arti = Number(this.state.saat.split(":")[1]) + 1;
      if (arti < 10) {
        arti = "0" + arti;
      }
      this.setState({ saat: this.state.saat.split(":")[0] + ":" + arti });
    }
    else {
      var basarti = Number(this.state.saat.split(":")[0]) + 1;
      if (basarti < 10) {
        basarti = "0" + basarti;
      }
      this.setState({ saat: basarti + ":" + "00" })
    }
    document.title = ney + this.state.saat;
  }
  render() {
    return (
      <>
        <div class="kapla"></div>
        <div class="oneGec">
          <h1>Pomodoro Timer</h1>
          <h2>The <b>Pomodoro Technique</b> is a time management method developed by Francesco Cirillo in the late 1980s. The technique uses a timer to break down work into intervals, traditionally 25 minutes in length, separated by short breaks...</h2>
          <Button variant="contained" color="primary" onClick={() => this.timer()}>{this.state.buttonDurum}</Button><saat>{this.state.saat}</saat>
          <hr />
          <div class="tour">
            {this.state.turList}<br /><br /><p><tour>Tour Number:</tour> {this.state.turSayi}</p>
          </div>
        </div>
      </>
    );
  }
}